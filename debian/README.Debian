64-bit Mshr/Dolfin
-------------

64-bit support is provided by python3-mshr64-real and libmshr64-dev.

64-bit here refers to 64-bit indexing provided by a 64-bit build of
PETSc, allowing handling of larger systems (larger meshes) with more
than 2 billion degress of freedom. It is provided in Dolfin by the
python3-dolfin64-real and libdolfin64-dev packages (libdolfin64.so).

This is activated by setting PETSC_DIR and SLEPC_DIR to appropriate paths,
e.g.
  PETSC_DIR=/usr/lib/petsc64
  SLEPC_DIR=/usr/lib/slepc64

Depending on usage you may also need to specific the dolfin libname
extension ("64") to link to libdolfin64.so rather than libdolfin.so.
This is controlled via DOLFIN_LIB_NAME_EXT. Likewise, linking with
libmshr64.so is facilitated via MSHR_LIB_NAME_EXT.
For cmake (C++ applications), set the cmake variable
  cmake -DMSHR_LIB_NAME_EXT=64 -DDOLFIN_LIB_NAME_EXT=64
For python, set it as an environment variable
  MSHR_LIB_NAME_EXT=64 DOLFIN_LIB_NAME_EXT=64 python3

Each of PETSC_DIR (SLEPC_DIR), MSHR_LIB_NAME_EXT and
DOLFIN_LIB_NAME_EXT need to be set to ensure the program links to
libpetsc64_*.so, libmshr64.so and libdolfin64.so, and not their 32-bit
counterparts.

python3-mshr is now a base package providing a mshr.pth pointing by
default at the preferred PETSc installation provided by
python3-mshr-real (for PETSC_DIR=/usr/lib/petsc
SLEPC_DIR=/usr/lib/slepc).
