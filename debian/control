Source: mshr
Section: math
Priority: optional
Maintainer: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Johannes Ring <johannr@simula.no>, Drew Parsons <dparsons@debian.org>
Build-Depends: debhelper-compat (= 13),
 python3-all-dev,
 dh-python,
 python3-setuptools,
 cmake (>= 3.5),
 python3-pybind11 (>= 2.2.2),
 libmpfr-dev,
 libgmp-dev,
 libcgal-dev (>= 4.13),
 libtet1.5-dev,
 libdolfin-dev (>= 2019.2),
 libdolfin64-dev (>= 2019.2.0~git20200218.027d9cc-16),
 python3-ffc (>= 2019.2),
 python3-numpy,
 python3-petsc4py,
 python3-slepc4py,
 python3-petsc4py-real,
 python3-slepc4py-real,
 python3-petsc4py-64-real,
 python3-slepc4py-64-real,
 libproj-dev
Standards-Version: 4.6.0
Homepage: http://fenicsproject.org
Vcs-Git: https://salsa.debian.org/science-team/fenics/mshr.git
Vcs-Browser: https://salsa.debian.org/science-team/fenics/mshr

Package: libmshr2019.2
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Shared libraries for mshr
 mshr generates simplicial DOLFIN meshes in 2D and 3D from geometries
 described by Constructive Solid Geometry (CSG) or from surface files,
 utilizing CGAL and Tetgen as mesh generation backends.
 .
 This package contains the shared libraries.

Package: libmshr-dev
Section: libdevel
Architecture: any
Depends: libmshr2019.2 (= ${binary:Version}),
 libmshr-dev-common (= ${binary:Version}),
 libdolfin-dev (>= ${fenics:Upstream-Version}),
 ${shlibs:Depends},
 ${misc:Depends}
Description: Shared links and header files for mshr
 mshr generates simplicial DOLFIN meshes in 2D and 3D from geometries
 described by Constructive Solid Geometry (CSG) or from surface files,
 utilizing CGAL and Tetgen as mesh generation backends.
 .
 This package contains the development files, and also the
 utility program mshrable.

Package: libmshr-dev-common
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: python3-ffc (>= ${fenics:Upstream-Version}),
 cmake (>= 2.8.0),
 libproj-dev,
 ${misc:Depends}
Recommends: libmshr-dev (= ${binary:Version}) | libmshr64-dev (= ${binary:Version})
Replaces: libmshr-dev (<< 2019.2.0~git20200213.8895485+dfsg1-3)
Breaks: libmshr-dev (<< 2019.2.0~git20200213.8895485+dfsg1-3)
Description: Shared links and header files for mshr
 mshr generates simplicial DOLFIN meshes in 2D and 3D from geometries
 described by Constructive Solid Geometry (CSG) or from surface files,
 utilizing CGAL and Tetgen as mesh generation backends.
 .
 This package contains the common headers and development files.

Package: python3-mshr
Section: python
Architecture: all
Depends: libmshr-dev (>= ${source:Version}),
 python3-mshr-real | python3-mshr64-real,
 python3-dolfin (>= ${fenics:Upstream-Version}),
 dolfin-bin (>= ${fenics:Upstream-Version}),
 python3,
 ${python3:Depends},
 ${misc:Depends}
Description: Python 3 interface for mshr
 mshr generates simplicial DOLFIN meshes in 2D and 3D from geometries
 described by Constructive Solid Geometry (CSG) or from surface files,
 utilizing CGAL and Tetgen as mesh generation backends.
 .
 This package contains the common Python 3 interface for mshr, and examples
 and demo programs.
 .
 This is the base package depending on specific mshr builds. By
 default mshr is built against dolfin using the preferred version of PETSc in
 /usr/lib/petsc (with 32-bit indexing), but the alternative version
 (64-bit PETSc) can be selected by setting the environment variable PETSC_DIR.

Package: python3-mshr-real
Section: python
Architecture: any
Depends: libmshr2019.2 (= ${binary:Version}),
 python3-dolfin-real (>= ${fenics:Upstream-Version}),
 python3-pybind11 (>= ${pybind11:Upstream-Version}), python3-pybind11 (<< ${pybind11:Next-Upstream-Version}),
 ${python3:Depends},
 ${shlibs:Depends},
 ${misc:Depends}
Recommends: python3-mshr
Description: Python 3 interface for mshr
 mshr generates simplicial DOLFIN meshes in 2D and 3D from geometries
 described by Constructive Solid Geometry (CSG) or from surface files,
 utilizing CGAL and Tetgen as mesh generation backends.
 .
 This package contains the Python 3 interface for mshr.

Package: libmshr64-2019.2
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Shared libraries for mshr with 64-bit indexing
 mshr generates simplicial DOLFIN meshes in 2D and 3D from geometries
 described by Constructive Solid Geometry (CSG) or from surface files,
 utilizing CGAL and Tetgen as mesh generation backends.
 .
 This package contains the shared libraries with support for
 64-bit indexing (libdolfin64 and PETSc-64).

Package: libmshr64-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libmshr64-2019.2 (= ${binary:Version}),
 libmshr-dev-common (= ${binary:Version}),
 libdolfin64-dev (>= ${fenics:Upstream-Version}),
 ${shlibs:Depends},
 ${misc:Depends}
Description: Shared links and header files for mshr with 64-bit indexing
 mshr generates simplicial DOLFIN meshes in 2D and 3D from geometries
 described by Constructive Solid Geometry (CSG) or from surface files,
 utilizing CGAL and Tetgen as mesh generation backends.
 .
 This package contains the development files with support for
 64-bit indexing (libdolfin64 and PETSc-64).

Package: python3-mshr64-real
Section: python
Architecture: any
Depends: libmshr64-2019.2 (= ${binary:Version}),
 python3-dolfin64-real (>= ${fenics:Upstream-Version}),
 python3-pybind11 (>= ${pybind11:Upstream-Version}), python3-pybind11 (<< ${pybind11:Next-Upstream-Version}),
 ${python3:Depends},
 ${shlibs:Depends},
 ${misc:Depends}
Recommends: python3-mshr
Description: Python 3 interface for mshr with 64-bit indexing
 mshr generates simplicial DOLFIN meshes in 2D and 3D from geometries
 described by Constructive Solid Geometry (CSG) or from surface files,
 utilizing CGAL and Tetgen as mesh generation backends.
 .
 This package contains the Python 3 interface for mshr with support for
 64-bit indexing (libdolfin64 and PETSc-64).
